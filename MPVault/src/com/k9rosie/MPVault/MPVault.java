package com.k9rosie.MPVault;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.k9rosie.MPVault.listeners.PlayerInteract;
import com.k9rosie.MPVault.listeners.SignChange;

public class MPVault extends JavaPlugin{
	private static final String YAML_CHEST_EXTENSION = ".chest.yml";
	
	private final HashMap<String, Inventory> chests = new HashMap<String, Inventory>();
	private File dataFolder;
	
	public void onEnable(){
		this.getLogger().info("MPVault enabled.");
		
		
		//configuration stuff
		File chestFolder = new File(getDataFolder(), "chests");
		dataFolder = getDataFolder();
		this.saveDefaultConfig();
		
		//register events
		PluginManager manager = this.getServer().getPluginManager();
		manager.registerEvents(new PlayerInteract(this), this);
		manager.registerEvents(new SignChange(this), this);
		load();
		final MPVaultCommands chestCommands = new MPVaultCommands(this);
		getCommand("vault").setExecutor(chestCommands);
		
	}
	public void onDisable(){
		this.getLogger().info("MPVault disabled.");
		int savedChests = this.save();
		this.getLogger().info("Saved " + savedChests + " chests");
	}
	public void load(){
		dataFolder.mkdirs();
		
		FilenameFilter filter = new FilenameFilter(){
			public boolean accept(File dir, String name){
				return name.endsWith(YAML_CHEST_EXTENSION);
			}
		};
		for (File chestFile : dataFolder.listFiles(filter)){
			String chestFileName = chestFile.getName();
			try{
				String playerName = chestFileName.substring(0, chestFileName.length() - 10);
				chests.put(playerName.toLowerCase(), loadFile(chestFile));
			}catch(Exception e){
				this.getLogger().info("Couldn't load chest file: " + chestFileName);
			}
		}
	}
	public int save(){
		int savedChests = 0;
		
		dataFolder.mkdirs();
		Iterator<Entry<String, Inventory>> chestIterator = chests.entrySet().iterator();
		while(chestIterator.hasNext()){
			final Entry<String, Inventory> entry = chestIterator.next();
			final String playerName = entry.getKey();
			final Inventory chest = entry.getValue();
			
			final File chestFile = new File(dataFolder, playerName + YAML_CHEST_EXTENSION);
			if(chest == null){
				chestFile.delete();
				chestIterator.remove();
			}else{
				try{
					saveFile(chest, chestFile);
					
					savedChests++;
				}catch(IOException e){
					this.getLogger().info("Couldn't save chest file: " + chestFile.getName());
				}
			}
		}
		return savedChests;
	}
	public void saveFile(Inventory inventory, File file) throws IOException{
		YamlConfiguration yaml = new YamlConfiguration();
		
		int inventorySize = inventory.getSize();
		yaml.set("size", inventorySize);
		
		ConfigurationSection items = yaml.createSection("items");
		for(int slot = 0; slot < inventorySize; slot++){
			ItemStack stack = inventory.getItem(slot);
			if(stack != null){
				items.set(String.valueOf(slot), stack);
			}
		}
		yaml.save(file);
	}
	public Inventory loadFile(File file) throws FileNotFoundException, IOException, InvalidConfigurationException{
		YamlConfiguration yaml = new YamlConfiguration();
		yaml.load(file);
		
		int inventorySize = yaml.getInt("size", 6 * 9);
		Inventory inventory = this.getServer().createInventory(null, inventorySize);
		
		ConfigurationSection items = yaml.getConfigurationSection("items");
		for(int slot = 0; slot < inventorySize; slot++){
			String slotString = String.valueOf(slot);
			if(items.isItemStack(slotString)){
				ItemStack itemStack = items.getItemStack(slotString);
				inventory.setItem(slot, itemStack);
			}
		}
		return inventory;
		
	}
	
	public Inventory getChest(String playerName){
		Inventory chest = chests.get(playerName.toLowerCase());
		
		if(chest == null){
			chest = this.getServer().createInventory(null, 6*9);
			chests.put(playerName.toLowerCase(), chest);
		}
		
		return chest;
	}
	
	public void removeChest(String playerName){
		chests.put(playerName.toLowerCase(), null);
	}
	public String colorize(String msg)
    {
        String coloredMsg = "";
        for(int i = 0; i < msg.length(); i++)
        {
            if(msg.charAt(i) == '&')
                coloredMsg += '§';
            else
                coloredMsg += msg.charAt(i);
        }
        return coloredMsg;
    }
}
